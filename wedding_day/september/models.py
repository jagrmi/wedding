from django.db import models
from django.contrib.auth.models import User

ALCOHOL = (
    ("All", "All"),
    ("Vodka", "Vodka"),
    ("Wine", "Wine"),
    ("Champagne", "Champagne"),
    ("Water", 'Water'),

)
PLUS_ONE = (
    ('Alone', 'I will be alone'),
    ('Husband/Wife', 'Husband/Wife'),
    ('Friend', 'Girl-friend/Boy-friend')
)
CHILD = (
    ('Without children', '0'),
    ('Boy < 3', 'Boy < 3'),
    ('Girl < 3', 'Girl <3'),
    ('3 < Boy < 10', '3 < Boy < 10'),
    ('3 < Girl < 10', '3 < Girl < 10'),
    ('Boy > 10', 'Boy > 10'),
    ('Girl > 10', 'Girl > 10')
)
FOOD = (
    ('All', 'All'),
    ('Not Fish', 'I do not eat fish'),
    ('Not Meet', 'I do not eat meet')
)
SECOND_DAY = (
    ('YES', 'Yes'),
    ('NO', 'No'),
    ('1/2', '1/2'),
    ('3/4', '3/4')
)


class Comment(models.Model):
    comment_description = models.CharField(max_length=20)

    def __str__(self):
        return self.comment_description


class AddGuest(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(blank=True, unique=True)
    plus_one = models.CharField(max_length=30, choices=PLUS_ONE, blank=True)
    first_child = models.CharField(choices=CHILD, blank=True, max_length=30)
    second_child = models.CharField(choices=CHILD, blank=True, max_length=30)
    alcohol = models.CharField(choices=ALCOHOL, max_length=30, blank=True)
    food = models.CharField(max_length=30, choices=FOOD, blank=True)
    second_day = models.CharField(max_length=30, choices=SECOND_DAY, blank=True)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

# Create your models here.
