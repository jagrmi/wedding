# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-05 09:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AddGuest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('email', models.EmailField(blank=True, max_length=254, unique=True)),
                ('plus_one', models.CharField(blank=True, choices=[('Alone', '1'), ('Husband/Wife', 'Husband/Wife'), ('Friend', 'Girl-friend/Boy-friend')], max_length=30)),
                ('first_child', models.CharField(blank=True, choices=[('Without children', '0'), ('Boy < 3', 'Boy < 3'), ('Girl < 3', 'Girl <3'), ('3 < Boy < 10', '3 < Boy < 10'), ('3 < Girl < 10', '3 < Girl < 10'), ('Boy > 10', 'Boy > 10'), ('Girl > 10', 'Girl > 10')], max_length=30)),
                ('second_child', models.CharField(blank=True, choices=[('Without children', '0'), ('Boy < 3', 'Boy < 3'), ('Girl < 3', 'Girl <3'), ('3 < Boy < 10', '3 < Boy < 10'), ('3 < Girl < 10', '3 < Girl < 10'), ('Boy > 10', 'Boy > 10'), ('Girl > 10', 'Girl > 10')], max_length=30)),
                ('alcohol', models.CharField(blank=True, choices=[('All', 'All'), ('Vodka', 'Vodka'), ('Wine', 'Wine'), ('Champagne', 'Champagne'), ('Water', 'Water')], max_length=30)),
                ('food', models.CharField(blank=True, choices=[('All', 'All'), ('Not Fish', 'I do not eat fish'), ('Not Meet', 'I do not eat meet')], max_length=30)),
                ('second_day', models.CharField(blank=True, choices=[('YES', 'Yes.'), ('NO', 'No.')], max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment_description', models.CharField(max_length=20)),
            ],
        ),
    ]
