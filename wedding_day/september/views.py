from django.shortcuts import render
from django.views.generic import TemplateView, CreateView
from .models import *
from django.urls import reverse_lazy


class HomeView(TemplateView):
    template_name = 'home'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        return context


class AddGuestsView(CreateView):
    model = AddGuest
    template_name = 'add_guest.html'
    fields = ('first_name', 'last_name', 'email', 'plus_one', 'first_child', 'second_child', 'alcohol', 'food',
              'second_day')
    initial = {'first_name': "First name",
               'last_name': 'Last name',
               'email': 'email@gmail.com',
               }
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(AddGuestsView, self).get_context_data(**kwargs)
        return context

    # def get_success_url(self):
    #     return reverse_lazy('home')
# Create your views here.
