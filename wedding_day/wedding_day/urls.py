"""wedding_day URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from september.views import *

urlpatterns = [
    url(r'^add/guest/$', AddGuestsView.as_view(), name='add_guest'),
    url(r'^sleep/$', TemplateView.as_view(template_name='sleep.html'), name='sleep'),
    url(r'^transport/$', TemplateView.as_view(template_name='transport.html'), name='transport'),
    url(r'^service/$', TemplateView.as_view(template_name='services.html'), name='service'),
    url(r'^story/$', TemplateView.as_view(template_name='404_ File not found.html'), name='story'),
    url(r'^contact/$', TemplateView.as_view(template_name='contact.html'), name='contact'),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^admin/', admin.site.urls),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
